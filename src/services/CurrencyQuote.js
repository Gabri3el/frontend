export default {
  data: {
    euro: 0,
    dolar: 0,
  },
  async getEuro() {
    await fetch("https://swop.cx/rest/rates/EUR/BRL", {
      method: "GET",
      headers: {
        Authorization:
          "ApiKey 56132eaead48a4add8692447428d48359254d9c182f489817543586e02fba73e",
      },
    })
      .then((res) => res.json())
      .then((response) => {
        this.euro = response.quote;
        return this.euro;
      });
  },
  async getDolar() {
    await fetch("https://swop.cx/rest/rates/USD/BRL", {
      method: "GET",
      headers: {
        Authorization:
          "ApiKey 56132eaead48a4add8692447428d48359254d9c182f489817543586e02fba73e",
      },
    })
      .then((res) => res.json())
      .then((response) => {
        this.dolar = response.quote;
        return this.dolar;
      });
  },
};
