<h1 align="center">LE / TIP | VueJS </h1>
<p align="center">Desafio Proposto pela Convenia para gerenciar gastos e convertendo para reais!!!</p>

<h1 align="center">
    <p>DESKTOP</p>
   <img src="./src/assets/Desktop.png" alt="Desktop-img" border="0"><br /><br />
   <p>Mobile</p>
   <img src="./src/assets/mobile.png" alt="mobile-img" border="0"><br /><br />
   <img src="./src/assets/mobile2.png" alt="mobile-img" border="0"><br /><br />

</h1>

<h1 align="center">
    <a href="https://br.vuejs.org/">🔗 VueJs</a>
</h1>

<p align="center">
 <a href="#Install">Install instructions</a> • 
 <a href="#tecnologias">Tecnologias</a> • 
 <a href="#autor">Autor</a>
</p>


## Install instructions

### Getting Started

#### 1) Clone & Install Dependencies on Linux/MacOS

- 1.1) `git clone `
- 1.2) `cd killerofmonster` - cd para entrar na pasta criada.
- 1.3)  Instale Pacotes NPM com `yarn install`

#### 2) Clone & Install Dependencies on Windows

- 2.1) `git clone`
- 2.2)  Abra o Windows Powershell como Administrador.
- 2.3) `cd killerofmonster` - cd para entrar na pasta criada.
- 2.4)  Instale Pacotes com `yarn install` ou `npm install`

#### 3) Start your app

- 3.1) $ npm run serve

## :zap: Tecnologias

<h1 align="center">
  <img src="https://pnglive.com/wp-content/uploads/2021/01/Vue-JS-Logo-Background-PNG-Image.png" alt="Stack" height="150" width="600">
  <br>
</h1>

-   [VueJs](https://github.com/vuejs)
-   [HTML]
-   [CSS]
-   [Javascript]


### Autor
---
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/62441006?v=4" width="100px;" alt=""/>
 <br />
 <sub><b>Gabriel Merino</b></sub></a> <a href="https://github.com/Gabri3el/" title="github">🚀</a>

 Feito com maior dedicação por Gabriel Merino!

[![Linkedin Badge](https://img.shields.io/badge/-Gabriel-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/gabrielmerinostos/)](https://www.linkedin.com/in/gabrielmerinostos/)
[![Gmail Badge](https://img.shields.io/badge/-gabrielmerino.dev@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:gabrielmerino.dev@gmail.com)](mailto:gabrielmerino.dev@gmail.com)
